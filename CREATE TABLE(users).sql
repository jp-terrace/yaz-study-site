﻿DROP TABLE users;
CREATE TABLE users( 
    id uuid DEFAULT gen_random_uuid(),          -- ユニークキー
    name character varying (16),                -- 氏名
    mailaddress character varying (256),        -- メールアドレス
    password character (255),                   -- パスワード
    birthday timestamp,                         -- 誕生日
    createDate timestamp,                       -- 登録年月日
    deleteFlg boolean                           -- 削除フラグ
);

