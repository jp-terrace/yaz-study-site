$(function() {
	$('.age-start-up').click(function() {
		ageUp($('.age-start'));
	})

	$('.age-start-down').click(function() {
		ageDown($('.age-start'));
	})

	$('.age-end-up').click(function() {
		ageUp($('.age-end'));
	})

	$('.age-end-down').click(function() {
		ageDown($('.age-end'));
	})

	$('.search-btn').click(function() {
		$('#formSearch').submit();
	})

	$('.textbox').keypress(function(e) {
		if (e.keyCode == 13) {
			$('#formSearch').submit();
		}
	})
});

function ageUp(target) {
	var age = Number($(target).val());

	if (isNumber(age) && age < 999) {
		age++;
		$(target).val(age);
	} else {
		$(target).val(18);
	}
}

function ageDown(target) {
	var age = Number($(target).val());

	if (isNumber(age) && age > 0) {
		age--;
		$(target).val(age);
	} else {
		$(target).val(18);
	}
}
