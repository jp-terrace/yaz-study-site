$(function () {
  $('.logout-btn').click(function () {
  	$('#formLogout').submit();
  })
});

/** 数値チェック */
function isNumber(numVal){
  var pattern = /^([1-9]\d*|0)$/;
  return pattern.test(numVal);
}