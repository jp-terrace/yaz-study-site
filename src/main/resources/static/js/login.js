var eyeOpenFlg = false;

$(function () {
	$('.login-btn').click(function () {
		$('#formLogin').submit();
	})

	$('.textbox').keypress(function(e) {
		if (e.keyCode == 13) {
			$('#formLogin').submit();
		}
	})
	
	$('.login-password-eye').click(function() {
		eyeOpenFlg = !eyeOpenFlg;
	  
		if(eyeOpenFlg) {
			$('#eye-open').removeClass('login-password-eye-none');
			$('#eye-close').addClass('login-password-eye-none');
			$('.login-password-text').attr('type', 'text');
		} else {
			$('#eye-open').addClass('login-password-eye-none');
			$('#eye-close').removeClass('login-password-eye-none');
			$('.login-password-text').attr('type', 'password');
		} 
	})	
});