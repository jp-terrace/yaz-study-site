package com.example.learn.entity;

import lombok.Data;

@Data
/**
 * 検索結果レスポンス
 */
public class SearchUserListResponse {
	/**
	 * ユーザーモデル
	 */
	private UserModel userModel;

	/**
	 * 年齢
	 */
	private Integer age;
}