package com.example.learn.entity;

import java.sql.Timestamp;
import java.util.UUID;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="users")
/**
 * ユーザー
 */
public class UserModel {
	
	@Id
	@GeneratedValue
	/**
	 * ユニークキー
	 */
	private UUID id;

	/**
	 * 氏名
	 */
	private String name;
	
	/**
	 * メールアドレス
	 */
	private String mailaddress;
	
	/**
	 * パスワード
	 */
	private String password;
	
	/**
	 * 誕生日
	 */
	private Timestamp birthday;
	
	/**
	 * 登録年月日
	 */
	private Timestamp createdate;
	
	/**
	 * 削除フラグ
	 */
	private Boolean deleteflg;
}
