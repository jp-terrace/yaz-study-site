package com.example.learn.entity;

import lombok.Data;

/**
 * ログインレスポンス
 */
@Data
public class LoginResponse {
	/**
	 * メッセージ
	 */
	private String msg;
}