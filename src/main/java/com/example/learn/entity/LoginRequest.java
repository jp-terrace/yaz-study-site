package com.example.learn.entity;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

/**
 * ログインリクエスト
 */
@Data
public class LoginRequest {
	/**
	 * メールアドレス 
	 */
	@NotEmpty
	@Email
	private String mailaddress;

	/**
	 * パスワード
	 */
	@NotEmpty
	private String password;
}