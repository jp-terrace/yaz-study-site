package com.example.learn.entity;

import lombok.Data;

/**
 * 検索条件リクエスト
 */
@Data
public class SearchRequest {
	/**
	 * メールアドレス 
	 */
	private String mailaddress;

	/**
	 * 氏名
	 */
	private String name;

	/**
	 * 年齢(下限)
	 */
	private String ageLower = "18";

	/**
	 * 年齢(上限)
	 */
	private String ageUpper = "18";

	/**
	 * 年齢(下限)を数値で取得する
	 * @return
	 */
	public Integer getAgeLowerNum() {
		try {
			return Integer.parseInt(this.ageLower);

		} catch (Exception ex) {
			return 0;
		}

	}

	/**
	 * 年齢(上限)を数値で取得する
	 * @return
	 */
	public Integer getAgeUpperNum() {
		try {
			return Integer.parseInt(this.ageUpper);

		} catch (Exception ex) {
			return 999;
		}
	}
}
