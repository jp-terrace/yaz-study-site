package com.example.learn.pagination;

import org.springframework.data.domain.Page;
import org.thymeleaf.util.NumberUtils;

public class PageInfo {
	public <T> Integer[] sequence(Page<T> page, int pageLinkMaxDispNum) {
		int begin = Math.max(0, page.getNumber() - pageLinkMaxDispNum / 2);
		int end = begin + (pageLinkMaxDispNum - 1);
		
		if (end > page.getTotalPages() - 1) {
			end = page.getTotalPages() - 1;
			begin = Math.max(0, end - (pageLinkMaxDispNum - 1));
			
			if(end < begin) {
				begin = end;
			}
		}

		return NumberUtils.sequence(begin, end);
	}
}