package com.example.learn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnDemoJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnDemoJpaApplication.class, args);
	}

}
