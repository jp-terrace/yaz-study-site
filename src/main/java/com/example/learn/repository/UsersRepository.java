package com.example.learn.repository;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.example.learn.entity.UserModel;

public interface UsersRepository extends JpaRepository<UserModel, UUID>, JpaSpecificationExecutor<UserModel> {
	public Page<UserModel> findAll(Pageable pageable);
}