package com.example.learn.specifications;

import org.springframework.data.jpa.domain.Specification;
import org.thymeleaf.util.StringUtils;

import com.example.learn.entity.UserModel;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

public class UserSpecifications {
	/**
	 * メールアドレス(完全一致)
	 */
	public static Specification<UserModel> mailaddressEqual(String value) {
		return StringUtils.isEmpty(value) ? null : new Specification<UserModel>() {
			@Override
			public Predicate toPredicate(Root<UserModel> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("mailaddress"), value);
			}
		};
	}

	/**
	 * メールアドレス(部分一致)
	 */
	public static Specification<UserModel> mailaddressContains(String value) {
		return StringUtils.isEmpty(value) ? null : new Specification<UserModel>() {
			@Override
			public Predicate toPredicate(Root<UserModel> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.like(root.get("mailaddress"), "%" + value + "%");
			}
		};
	}

	/**
	 * 氏名(部分一致)
	 */
	public static Specification<UserModel> nameContains(String value) {
		return StringUtils.isEmpty(value) ? null : new Specification<UserModel>() {
			@Override
			public Predicate toPredicate(Root<UserModel> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.like(root.get("name"), "%" + value + "%");
			}
		};
	}

	/**
	 * 年齢(範囲)
	 */
	public static Specification<UserModel> ageBetween(Integer lower, Integer upper) {
		return new Specification<UserModel>() {
			@Override
			public Predicate toPredicate(Root<UserModel> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				// 誕生日を年齢に変換
				Expression<Integer> age = cb.function("age", Integer.class, root.get("birthday"));

				// 年齢を年に変換
				Expression<Integer> birthday = cb.function("date_part", Integer.class,
						cb.literal("year"), age);

				// 範囲抽出
				return cb.between(birthday, lower, upper);
			}
		};
	}
	
	/**
	 * パスワード(完全一致)
	 */
	public static Specification<UserModel> passwordEqual(String value) {
		return StringUtils.isEmpty(value) ? null : new Specification<UserModel>() {
			@Override
			public Predicate toPredicate(Root<UserModel> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("password"), value);
			}
		};
	}
}
