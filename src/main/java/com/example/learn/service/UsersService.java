package com.example.learn.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.example.learn.entity.UserModel;
import com.example.learn.repository.UsersRepository;
import com.example.learn.specifications.UserSpecifications;

@Service
public class UsersService {
	@Autowired
	private UsersRepository usersRepository;

	/**
	 * 全件取得
	 * @return
	 */
	public Page<UserModel> findAll(Pageable pageable) {
		return this.usersRepository.findAll(pageable);
	}


	/**
	 * ユーザー取得
	 * @param mailaddress メールアドレス
	 * @return
	 */
	public Optional<UserModel> findUser(String mailaddress) {
		return this.usersRepository.findOne(Specification
				.where(UserSpecifications.mailaddressEqual(mailaddress)));
	}
	
	/**
	 * ユーザー検索
	 * @param mailaddress メールアドレス
	 * @param name 氏名
	 * @param ageLower 年齢(下限)
	 * @param ageUpper 年齢(上限)
	 * @param pageable ページネーション
	 * @return
	 */
	public Page<UserModel> findUsers(String mailaddress, String name, Integer ageLower, Integer ageUpper, Pageable pageable) {
		return this.usersRepository.findAll(Specification
				.where(UserSpecifications.mailaddressContains(mailaddress))
				.and(UserSpecifications.nameContains(name))
				.and(UserSpecifications.ageBetween(ageLower, ageUpper)), pageable);
	}
}