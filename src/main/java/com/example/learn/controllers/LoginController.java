package com.example.learn.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.learn.entity.LoginRequest;
import com.example.learn.entity.LoginResponse;
import com.example.learn.entity.UserModel;
import com.example.learn.service.UsersService;

import jakarta.validation.Valid;

/**
 * ログイン画面
 */
@Controller
public class LoginController {
	@Autowired
	private UsersService usersService;

	/**
	 * 初期表示
	 * @return
	 */
	@GetMapping("/login")
	public ModelAndView view(@ModelAttribute LoginRequest loginRequest) {
		ModelAndView model = new ModelAndView();
		model.setViewName("login");
		return model;
	}

	/**
	 * ログイン
	 * @return
	 */
	@PostMapping("/login")
	public ModelAndView login(@Valid LoginRequest loginRequest, BindingResult result) {
		LoginResponse response = new LoginResponse();
		ModelAndView model = new ModelAndView();
		model.addObject("loginResponse", response);

		// バリデーションチェック
		if (result.hasErrors()) {
			return model;
		}

		// ユーザー取得
		Optional<UserModel> user = usersService.findUser(loginRequest.getMailaddress());

		// ユーザーが存在する、かつパスワードが一致
		if (user.isPresent() && user.get().getPassword().equals(loginRequest.getPassword())) {
			// 遷移先をトップ画面に設定
			model.setViewName("top");
		}
		// 認証NG
		else {
			// 遷移先をログイン画面に設定
			model.setViewName("login");

			// メッセージ設定
			response.setMsg("メールアドレス、またはパスワードが誤っています");
		}

		return model;
	}
}