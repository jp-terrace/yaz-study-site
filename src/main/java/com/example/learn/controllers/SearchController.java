package com.example.learn.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import com.example.learn.entity.SearchRequest;
import com.example.learn.entity.SearchUserListResponse;
import com.example.learn.entity.UserModel;
import com.example.learn.pagination.PageInfo;
import com.example.learn.service.UsersService;

/**
 * 検索画面
 */
@Controller
public class SearchController {
	@Autowired
	private UsersService usersService;

	/**
	 * 初期表示
	 * @param searchRequest リクエスト
	 * @return
	 */
	@GetMapping("/search")
	public ModelAndView view(@ModelAttribute SearchRequest searchRequest, Pageable pageable) {
		ModelAndView model = new ModelAndView();
		model.setViewName("search");

		// 検索
		Page<UserModel> pagination = usersService.findUsers(
				searchRequest.getMailaddress(),
				searchRequest.getName(),
				searchRequest.getAgeLowerNum(),
				searchRequest.getAgeUpperNum(),
				pageable);

		// 検索結果モデル
		List<SearchUserListResponse> searchUsers = new ArrayList<SearchUserListResponse>();

		for (UserModel user : pagination) {
			SearchUserListResponse item = new SearchUserListResponse();

			// ユーザー情報
			item.setUserModel(user);

			// 年齢
			item.setAge(this.calcAge(new Date(user.getBirthday().getTime())));

			searchUsers.add(item);
		}

		PageInfo pageInfo = new PageInfo();
		
		model.addObject("pageInfo", pageInfo);
		model.addObject("pagination", pagination);
		model.addObject("searchUsers", searchUsers);

		return model;

	}

	/**
	 * 年齢計算
	 * @param birthday
	 * @return
	 */
	private int calcAge(Date birthday) {
		// Calendar型のインスタンス生成
		Calendar calendarBirth = Calendar.getInstance();
		Calendar calendarNow = Calendar.getInstance();

		// Date型をCalendar型に変換する
		calendarBirth.setTime(birthday);
		calendarNow.setTime(new Date());

		// （現在年ー生まれ年）で年齢の計算
		int age = calendarNow.get(Calendar.YEAR) - calendarBirth.get(Calendar.YEAR);

		// 誕生月を迎えていなければ年齢-1
		if (calendarNow.get(Calendar.MONTH) < calendarBirth.get(Calendar.MONTH)) {
			age -= 1;
		} else if (calendarNow.get(Calendar.MONTH) == calendarBirth.get(Calendar.MONTH)) {
			// 誕生月は迎えているが、誕生日を迎えていなければ年齢−１
			if (calendarNow.get(Calendar.DATE) < calendarBirth.get(Calendar.DATE)) {
				age -= 1;
			}
		}

		return age;
	}
}