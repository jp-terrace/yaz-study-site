package com.example.learn.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * トップ画面
 */
@Controller
public class TopController {
	/**
	 * 初期表示
	 * @return
	 */
	@GetMapping("/top")
	public ModelAndView view() {
		ModelAndView model = new ModelAndView();
		model.setViewName("top");
		return model;
	}
}